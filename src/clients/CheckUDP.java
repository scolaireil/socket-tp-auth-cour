package clients;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import javax.json.*; 
 

public class CheckUDP {

    public static void main(String[] args) throws IOException {
   
        // construction de l'adresse et du port du serveur
        // construction de la socket
          DatagramSocket ds = new DatagramSocket();
         // construction de l'adresse et du port du serveur
            InetAddress adrser = InetAddress.getByName("localhost");
            int port =5500;
            // saisir login + mot de passe
            
            // construction de la requÃªte (chaÃ®ne de caractÃ¨res)
            String req = "CHK Toto Toto";
            System.out.println(req);
            // construction du DatagramPacket pour envoyer la requÃªte
            DatagramPacket dp_out = new DatagramPacket( req.getBytes(), req.getBytes().length , adrser , port);
            // Ã©mission de la requÃªte
            ds.send(dp_out);
            
            
            System.out.println("Messge envoyé : " + req);
            // tampon pour recevoir les rÃ©ponses
            byte[] tampon = new byte[1024];
            // construction du DatagramPacket pour recevoir la rÃ©ponse
            
            DatagramPacket dp_in = new DatagramPacket(tampon,tampon.length); 
            // attente de la rÃ©ponse
            ds.receive(dp_in);
            // affiche la rÃ©ponse
            String rep = new String(dp_in.getData(),0,dp_in.getLength()); 
            System.out.println("Reponse : " + rep);
            ds.close();
         
    }
}