/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package as;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
 

/**
 *
 * @author Ordi
 */


 // objet ListeAuth pour tester le couple login/mdp
   
public class AuthServerUDP  extends Thread {
    
    
    private ListeAuth la; 
    
    public AuthServerUDP(ListeAuth la)  {

      this.la  = la;
    }
    
    public void run()
    { 
        try{
              this.traiterRequetes();
        }catch(IOException ex)
        {
            ex.printStackTrace();
        }
        
    }
    
    
    
      public void traiterRequetes()throws SocketException  {

        // creation DatagramSocket
        DatagramSocket de = new DatagramSocket(5500);
        // tampon pour recevoir la requÃªte
        byte [] tampon = new byte [1024];
        // objet DatagramPacket servant Ã  recevoir des requÃªtes
        DatagramPacket dp= new DatagramPacket(tampon,tampon.length) ;
         
      

        while(true) {
            try {
                // attends une requÃªte
                System.out.println("**** Attente de requêtes ...");
                
                de.receive(dp);
                // transforme le tampon en chaÃ®ne de caractÃ¨re
                 String req =  new String(dp.getData(),0, dp.getLength()); 
                // traite la requÃªte : dÃ©coupage, vÃ©rification type...
                System.out.println("Requete reçue : "+req);
                
                String [] tab = req.split(" "); 
                // tab[0] --> CHK , tab[1] --> login ; tab[2] --> mot de passe 
                String rep; 
                
                switch(tab[0])
                {
                    
                     // traitement ListeAuth avec appel mÃ©thode tester()
                    case"CHK": 
                          if (this.la.tester(tab[1], tab[2])) {
                           rep = "GOOD"; 
                           }
                          else{
                              rep="BAD";
                          }
                        
                    
                    default :
                        rep = "Type de protocole"; 
                  
                }
                
               
            
                // construction DatagramPacket pour rÃ©ponse
             dp.setData(rep.getBytes());
                // renvoie la rÃ©ponse au client
             de.send(dp);
                System.out.println("Réponse renvoyée ");
                //rénisialiser   le tampon avec mémoire tampon (1024) 
                //obligation  car mêmee 
                dp.setData(tampon);
            } catch (IOException e) {
                // gestion d'erreur
                e.printStackTrace();
                de.close(); 
            }
        }
    
      }


  public static void main(String [] args) throws SocketException {
        AuthServerUDP s = new AuthServerUDP(new ListeAuth("authentif"));
        s.traiterRequetes();
    }
}