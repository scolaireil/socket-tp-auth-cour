/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package as;

import java.net.SocketException;
import java.io.IOException;
/**
 *
 * @author Ordi
 */
public class AuthServerMultiProto {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)  {
        // TODO code application logic here
        
        ListeAuth la = new ListeAuth("authentif");
          
        AuthServerTCP tcpServer = new AuthServerTCP(la);
        AuthServerUDP udpServer = new AuthServerUDP(la);
      
        
        tcpServer.start();
        udpServer.start();

    }
    
}
