/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package as;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
//import java.net.SocketException;

/**
 *
 * @author Ordi
 */


 // objet ListeAuth pour tester le couple login/mdp
   
public class AuthServerTCP  extends Thread {
    
    
    private ListeAuth la; 
    
    public AuthServerTCP(ListeAuth la)  {

      this.la  = la;
    }
     
    public void run()
    { 
        try{
            this.traiterRequetes();
        }catch(IOException ex)
        {
            ex.printStackTrace();
        }
     
    }
    
    
      public void traiterRequetes()throws  IOException  {

        // creation ServerSocket 
        ServerSocket socketEcoute = new ServerSocket(5500);
        // tampon pour recevoir la requÃªte
    
        // objet DatagramPacket servant Ã  recevoir des requÃªtes
        

        while(true) {
            try {
                // attends une requÃªte
                System.out.println("**** Attente de requêtes ...");
                Socket sock = socketEcoute.accept(); //function bloquante 
           
                // recuptere les flux E/S 
           BufferedReader br =  new BufferedReader(new InputStreamReader(sock.getInputStream()));
           PrintWriter pw = new PrintWriter(sock.getOutputStream(),true);
                // traite lecture requette
               String req =  br.readLine();
              
                System.out.println("Requete recue : " + req);
                String [] tab = req.split(" "); 
                // tab[0] --> CHK , tab[1] --> login ; tab[2] --> mot de passe 
                String rep; 
                switch(tab[0])
                {
                     // traitement ListeAuth avec appel mÃ©thode tester()
                    case"CHK": 
                          if (this.la.tester(tab[1], tab[2])) {
                           rep = "GOOD"; 
                           }
                          else{
                              rep="BAD";
                          }
                    break; 
                    
                    default :
                        rep = "Type de protocole"; 
                }
                
               
            
                // construction  rÃ©ponse
              
 
               pw.println(rep);
                // renvoie la rÃ©ponse au client
               
                while (true) {                    
                         JsonLogger.log(sock.getInetAddress().toString(),sock.getPort(),"TCP",tab[0],tab[1], rep );
                    System.out.println("Réponse renvoyée ");
                }
           
                
               
           
                
               
                //obligation  car mêmee 
            } catch (IOException e) {
                // gestion d'erreur
                
                   
                e.printStackTrace();
            socketEcoute.close();
      
            }
        }
      
    
      }


  public static void main(String [] args) throws IOException{
        AuthServerTCP s = new AuthServerTCP(new ListeAuth("authentif"));
    
        s.traiterRequetes();
    }
}