# README #

##  introduction 


nous avons mis en place des serveurs pour avoir des transactions tcpsuite cela nous devons intégrer un Logger.


comme le serveur d'authentification (récepteur) entre le serveur client (émetteur).
Je me suis servi du serveur d'authentification (émetteur) comme le client et serveur loggeur (récepteur).
### avant

1. serveur Authifiction TCP
2. serveur Authifiction UDP
3. serveur client qui des requête TCP
4. serveur Authifiction des  requête UDP

### aprés 

1. serveur Authifiction TCP
2. serveur Authifiction UDP
3. serveur client qui des requête TCP
4. serveur Authifiction des  requête UDP
5. serveur Loggeur  TCP 


## Representation
![apres](./visuel/representation.png) 

---

## Code 

> Logger AuthLogger.java 

serveur AuthLogger qui réagir au serveur Authtcp (qui est cliente) qui  rémmet JsonLogger



```Java
    public void traiterRequetes()throws  IOException  {

        // creation ServerSocket 
        ServerSocket socketEcoute = new ServerSocket(5501);
        // tampon pour recevoir la requÃªte
    
        // objet DatagramPacket servant Ã  recevoir des requÃªtes        
      while(true)
      {
            try {
                // attends une requÃªte
                System.out.println("**** Attente de requêtes ...");
                Socket sock = socketEcoute.accept(); //function bloquante 
                // recuptere les flux E/S 
                BufferedReader br =  new BufferedReader(new InputStreamReader(sock.getInputStream()));
                PrintWriter pw = new PrintWriter(sock.getOutputStream(),true);
                // traite lecture requett
                // while true si optimiser
                String req =  br.readLine();
                System.out.println("Requete recue : " + req);
               
                sock.close();
                //obligation  car mêmee 
            } catch (IOException e) {
                // gestion d'erreur
                e.printStackTrace();
            socketEcoute.close();
            }
        }
      }


```
---

> JsonLogger.java

serveur JsonLogger avec un  socket qui attend l'envoi Authlogger

```Java
        public static void log(String host, int port, String proto, String type, String login, String result) {
		JsonLogger logger = getLogger();
		// à compléter
                try{ 
               Socket sock =  new Socket("localhost", 5501);
        // recupere les flux entree/sortie de la socket 
               BufferedReader br =  new BufferedReader(new InputStreamReader(sock.getInputStream()));
               PrintWriter pw = new PrintWriter(sock.getOutputStream(),true);
               String jsonreq=  logger.reqToJson(host, port, proto, type, login, result).toString();
               pw.println(jsonreq);
                 sock.close(); 
           }catch( Exception ex )
           {
                ex.printStackTrace();
           }
              
	}

```


## Conclusion 

cela m'a permi de comprendre : 
- les interactions entre les serveurs localhost mais, aussi distant
- les protocoles et leur transction 
- les playload ou charge utilie 
- cela ma pousser de comprendre les charges utiles  pour ssh,ftp, smb ,http 
